/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1; /* -b  option; if 0, dmenu appears at bottom */
static int fuzzy = 1; /* -F  option; if 0, dmenu doesn't use fuzzy matching */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Roboto:size=11",
};
static const char *prompt = NULL; /* -p  option; prompt to the left of input field */

#include "themes/ghyam.h"
static const char *colors[SchemeLast][2] = {
	/*								  fg         bg       */
	[SchemeNorm] = { col_norm_fg, col_norm_bg },
	[SchemeSel]	 = { col_sel_fg, col_sel_bg },
	[SchemeOut]  = { col_out_fg, col_out_bg },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
